<?php
ini_set("display_errors", "On");
require __DIR__ . '/vendor/autoload.php';

define("BASE_DIR", __DIR__);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Handlers\Strategies\RequestResponseArgs;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;


$app = AppFactory::create();
$app->setBasePath("/bucket/index.php");

$twig = Twig::create(__DIR__.'/templates', ['cache' => false]);

$app->addRoutingMiddleware();
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$routeCollector = $app->getRouteCollector();
$routeCollector->setDefaultInvocationStrategy(new RequestResponseArgs());

$app->add(TwigMiddleware::create($app, $twig));

require  __DIR__ . '/routes.php';

$app->run();