<?php

namespace Bucket\Encrypt;


class Encrypt
{
    static function safeEncrypt(string $message, string $key): string
    {
        return openssl_encrypt($message,"AES-128-ECB",$key);
    }

    /**
     * Decrypt a message
     * 
     * @param string $encrypted - message encrypted with safeEncrypt()
     * @param string $key - encryption key
     * @return string
     * @throws Exception
     */
    static function safeDecrypt(string $encrypted, string $key): string
    {   
        return openssl_decrypt($encrypted,"AES-128-ECB",$key);
    }
}