<?php

namespace Bucket\Controller;


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Bucket\Encrypt\Encrypt;
use Slim\Views\Twig;


class Main extends Controller
{
    protected $key = "HEXAGON905";

    public function index(Request $request, Response $response)
    {
        $this->view = Twig::fromRequest($request);

        $objects = $this->s3->getIterator('ListObjects', array(
            "Bucket" => $this->bucket,
        ));

        return $this->view->render($response, 'index.html', ["objects" => $objects]);
    }

    public function saveFile(Request $request, Response $response)
    {
        $this->view = Twig::fromRequest($request);

        $method = $request->getMethod();

        if($method == "POST")
        {
            $params = (array) $request->getParsedBody();
            $name = md5(rand(90, 100)).".txt";


            // Send a PutObject request and get the result object.
            $insert = $this->s3->putObject([
                'Bucket' => $this->bucket,
                'Key'    => $name,
                'Body'   => Encrypt::safeEncrypt($params["content"], $this->key) 
            ]);

            // Download the contents of the object.
            $retrive = $this->s3->getObject([
                'Bucket' => $this->bucket,
                'Key'    => $name,
                'SaveAs' => BASE_DIR."/cache/files/".$name."_local"
            ]);

            $data = Encrypt::safeDecrypt($retrive["Body"], $this->key);

            $payload = json_encode([
                "file_name" => $name
            ]);

            $response->getBody()->write($payload);

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        }
    }


    public function openFile(Request $request, Response $response, $name)
    {
        $this->view = Twig::fromRequest($request);

        if(!empty($name))
        {
            $retrive = $this->s3->getObject([
                'Bucket' => $this->bucket,
                'Key'    => $name,
                'SaveAs' => BASE_DIR."/cache/files/".$name."_local"
            ]);

            $data = Encrypt::safeDecrypt($retrive["Body"], $this->key);

            return $this->view->render($response, 'show.html', ["data" => $data]);
        }
    }

    public function listFiles(Request $request, Response $response)
    {
        $this->view = Twig::fromRequest($request);

        $objects = $this->s3->getIterator('ListObjects', array(
            "Bucket" => $this->bucket,
        ));

        $payload = json_encode(["objects" => $objects]);

        $response->getBody()->write($payload);

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }
}