<?php

namespace Bucket\Controller;

use Slim\Views\Twig;
use Aws\S3\S3Client;

class Controller
{
    public $s3;
    public $bucket;

    public function __construct() {
        $config = require(__DIR__."/../../config.php");
        $this->bucket = $config["bucket"];
        // Instantiate an Amazon S3 client.
        $this->s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'endpoint' => $config["endpoint"],
            'use_path_style_endpoint' => true,
            'credentials' => $config["credentials"],
        ]);
    }
}