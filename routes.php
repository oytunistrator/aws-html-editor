<?php
use Bucket\Controller\Main;


$app->get('', [Main::class, 'index']);

$app->post('/save', [Main::class, 'saveFile']);

$app->get('/show/{name}', [Main::class, 'openFile']);

$app->get('/list', [Main::class, 'listFiles']);